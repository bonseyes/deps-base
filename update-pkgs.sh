#!/bin/bash

set -e
set -x

submodules=$(git config --file .gitmodules --get-regexp path | awk '{ print $2 }' | grep -v "pkg-armcl\|pkg-protobuf")

git submodule init
git submodule sync
git submodule update --init --recursive $submodules

git submodule update pkg-armcl pkg-protobuf

cd pkg-armcl
git submodule init
git submodule sync
git submodule update --init

cd ../pkg-protobuf
git submodule init
git submodule sync
git submodule update --init
cd ..
